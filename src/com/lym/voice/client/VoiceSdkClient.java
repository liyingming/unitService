package com.lym.voice.client;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.baidu.aip.speech.AipSpeech;
import com.baidu.aip.speech.TtsResponse;

/**
 * 语音服务功能核心类 
 * @author liyingming
 * @Jautodoc 荒墨丶迷失  百度AI社区版主 
 */
public class VoiceSdkClient {

    //设置APPID/AK/SK 如有问题 请联系 
		public   final String APP_ID = "";
	    public   final String API_KEY = "";
	    public  final  String SECRET_KEY = "";
    
    // 初始化一个Client
    AipSpeech client = new AipSpeech(APP_ID, API_KEY, SECRET_KEY);
    
    /**
     * 语音识别(javaSDK调用)
     * @param data
     * @return
     */
    public String getVoiceBySdk(byte[] data){
    	// 可选：设置网络连接参数
    	client.setConnectionTimeoutInMillis(2000); //建立连接的超时时间（单位：毫秒）
        client.setSocketTimeoutInMillis(60000); //通过打开的连接传输数据的超时时间（单位：毫秒）
    	// 对语音二进制数据进行识别
    	JSONObject asrRes = client.asr(data, "pcm", 8000, null);
        System.out.println(asrRes);
        String Rtext = asrRes.optString("result");
        return Rtext.replaceAll("[^\\u4e00-\\u9fa5]", "");
    }
    
    /**
     * 语音合成(javaSDK调用)
     * @param text
     * @param request
     * @return
     */
    public byte[] synthesis(String text,HttpServletRequest request){
    	// 设置可选参数
    	HashMap<String, Object> options = new HashMap<String, Object>();
        options.put("spd", "5");//语速 取值0-9，默认为5中语速
        options.put("pit", "5");//音调 取值0-9，默认为5中语调
        options.put("per", "4");//发音人选择, 0为女声，1为男声，3为情感合成-度逍遥，4为情感合成-度丫丫，默认为普通女
    	
    	//文字 使用UTF-8编码，请注意文本长度必须小于1024字节
    	TtsResponse res = client.synthesis(text, "zh", 1, options);
        JSONObject result = res.getResult(); //服务器返回的内容，合成成功时为null,失败时包含error_no等信息
        if(result==null){
        	//合成成功
        	byte[] data = res.getData();
        	System.out.println(data);
    		return data;
        }else{
        	//合成失败
        	System.out.println(result);
        	return null;
        }
    }
    
}
