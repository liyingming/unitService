package com.lym.unit.controller;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.baidu.aip.util.Base64Util;
import com.lym.unit.client.UnitService;
import com.lym.voice.client.VoiceSdkClient;
/**
 * 智能问答对话系统
 * @author liyingming
 * @data 2019-5-23
 */
@Controller
@RequestMapping(value = "/unit")
public class UnitServiceController {
	
	//语音识别核心类 (javaSDK调用)
	VoiceSdkClient vsc = new VoiceSdkClient(); 
	
	/**
	 * 对话首页
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/index.do")
	public ModelAndView queryUnit() throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/unit/index");
		return modelAndView;
	}
	
	
	/**
	 * 语音识别获取机器人
	 * @param audioData
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveVocie.do")
	@ResponseBody
	public Map<String, Object> save(@RequestParam MultipartFile audioData,String Rtext) throws Exception {
		//注意 文件流接受  MultipartFile
		Map<String, Object> modelMap = new HashMap<String, Object>();
		try {
			//音频文件 转化为 byte[]
			InputStream content = audioData.getInputStream();
			ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
			byte[] buff = new byte[100];
			int rc = 0;
			while ((rc = content.read(buff, 0, 100)) > 0) {
				swapStream.write(buff, 0, rc);
			}
			// 获得二进制数组
			byte[] byte1 = swapStream.toByteArray();
			Rtext = vsc.getVoiceBySdk(byte1);
		    System.out.println("语音识别内容:"+Rtext);
			modelMap.put("Rtext", Rtext);
			modelMap.put("success", true);
        } catch (Exception e) {
            e.printStackTrace();
            modelMap.put("success", false);
			modelMap.put("data", e.getMessage());
        }
		return modelMap;
	}
	

	/**
	 * 文本获取机器人回复
	 * @author liyingming
	 * @return
	 */
	@RequestMapping(value = "/getBot.do")
	@ResponseBody
	public Map<String, Object> save(String msg,HttpServletRequest request){
		Map<String, Object> modelMap = new HashMap<String, Object>();
		try {
			String say ="你想说些什么呢？";
			if(msg!=null && msg !="") {
				//UNIT意图理解
				say = UnitService.utterance(msg);
			}
			modelMap.put("success", true);
			//返回机器人说的bot
			modelMap.put("say", say );
			byte[] data = vsc.synthesis(say,request);//合成需要播放的音频二进制数据
			String base64Data = Base64Util.encode(data);
			modelMap.put("base64Data", base64Data);
			modelMap.put("success", true);
        } catch (Exception e) {
            e.printStackTrace();
            modelMap.put("success", false);
            modelMap.put("say", "服务器连接失败～请稍后再试！");
			modelMap.put("data", e.getMessage());
        }
		return modelMap;
	}
	
}
