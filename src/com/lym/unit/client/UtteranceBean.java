package com.lym.unit.client;

import java.util.Date;
import java.util.List;

/**
 * Auto-generated: 2019-05-29 17:31:2
 *
 * @author liyingming
 *
 */
public class UtteranceBean {

	private Result result;
	private int error_code;
	private String error_msg;
	public void setResult(Result result) {
		this.result = result;
	}
	public Result getResult() {
		return result;
	}

	public void setError_code(int error_code) {
		this.error_code = error_code;
	}
	public int getError_code() {
		return error_code;
	}

	public void setError_msg(String error_msg) {
		this.error_msg = error_msg;
	}
	public String getError_msg() {
		return error_msg;
	}


	public static class Schema {

		private int intent_confidence;
		private List<String> slots;
		private int domain_confidence;
		private String intent;
		public void setIntent_confidence(int intent_confidence) {
			this.intent_confidence = intent_confidence;
		}
		public int getIntent_confidence() {
			return intent_confidence;
		}

		public void setSlots(List<String> slots) {
			this.slots = slots;
		}
		public List<String> getSlots() {
			return slots;
		}

		public void setDomain_confidence(int domain_confidence) {
			this.domain_confidence = domain_confidence;
		}
		public int getDomain_confidence() {
			return domain_confidence;
		}

		public void setIntent(String intent) {
			this.intent = intent;
		}
		public String getIntent() {
			return intent;
		}

	}



	public static class Qu_res {

	}


	public static class Refine_detail {

		private List<String> option_list;
		private String interact;
		private String clarify_reason;
		public void setOption_list(List<String> option_list) {
			this.option_list = option_list;
		}
		public List<String> getOption_list() {
			return option_list;
		}

		public void setInteract(String interact) {
			this.interact = interact;
		}
		public String getInteract() {
			return interact;
		}

		public void setClarify_reason(String clarify_reason) {
			this.clarify_reason = clarify_reason;
		}
		public String getClarify_reason() {
			return clarify_reason;
		}

	}


	public static class Action_list {

		private String action_id;
		private Refine_detail refine_detail;
		private int confidence;
		private String custom_reply;
		private String say;
		private String type;
		public void setAction_id(String action_id) {
			this.action_id = action_id;
		}
		public String getAction_id() {
			return action_id;
		}

		public void setRefine_detail(Refine_detail refine_detail) {
			this.refine_detail = refine_detail;
		}
		public Refine_detail getRefine_detail() {
			return refine_detail;
		}

		public void setConfidence(int confidence) {
			this.confidence = confidence;
		}
		public int getConfidence() {
			return confidence;
		}

		public void setCustom_reply(String custom_reply) {
			this.custom_reply = custom_reply;
		}
		public String getCustom_reply() {
			return custom_reply;
		}

		public void setSay(String say) {
			this.say = say;
		}
		public String getSay() {
			return say;
		}

		public void setType(String type) {
			this.type = type;
		}
		public String getType() {
			return type;
		}

	}


	public static class Response {

		private String msg;
		private Schema schema;
		private Qu_res qu_res;
		private List<Action_list> action_list;
		private int status;
		public void setMsg(String msg) {
			this.msg = msg;
		}
		public String getMsg() {
			return msg;
		}

		public void setSchema(Schema schema) {
			this.schema = schema;
		}
		public Schema getSchema() {
			return schema;
		}

		public void setQu_res(Qu_res qu_res) {
			this.qu_res = qu_res;
		}
		public Qu_res getQu_res() {
			return qu_res;
		}

		public void setAction_list(List<Action_list> action_list) {
			this.action_list = action_list;
		}
		public List<Action_list> getAction_list() {
			return action_list;
		}

		public void setStatus(int status) {
			this.status = status;
		}
		public int getStatus() {
			return status;
		}

	}


	public static class Result {

		private String version;
		private Date timestamp;
		private Response response;
		private String bot_id;
		private String log_id;
		private String bot_session;
		private String interaction_id;
		public void setVersion(String version) {
			this.version = version;
		}
		public String getVersion() {
			return version;
		}

		public void setTimestamp(Date timestamp) {
			this.timestamp = timestamp;
		}
		public Date getTimestamp() {
			return timestamp;
		}

		public void setResponse(Response response) {
			this.response = response;
		}
		public Response getResponse() {
			return response;
		}

		public void setBot_id(String bot_id) {
			this.bot_id = bot_id;
		}
		public String getBot_id() {
			return bot_id;
		}

		public void setLog_id(String log_id) {
			this.log_id = log_id;
		}
		public String getLog_id() {
			return log_id;
		}

		public void setBot_session(String bot_session) {
			this.bot_session = bot_session;
		}
		public String getBot_session() {
			return bot_session;
		}

		public void setInteraction_id(String interaction_id) {
			this.interaction_id = interaction_id;
		}
		public String getInteraction_id() {
			return interaction_id;
		}

	}

}