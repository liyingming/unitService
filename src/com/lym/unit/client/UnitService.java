package com.lym.unit.client;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lym.common.util.HttpUtil;
import com.lym.unit.client.UtteranceBean.Action_list;
import com.lym.unit.client.UtteranceBean.Result;
/*
 * unit对话服务
 */
public class UnitService {
    /**
     * 重要提示代码中所需工具类
     * FileUtil,Base64Util,HttpUtil,GsonUtils请从
     * https://ai.baidu.com/file/658A35ABAB2D404FBF903F64D47C1F72
     * https://ai.baidu.com/file/C8D81F3301E24D2892968F09AE1AD6E2
     * https://ai.baidu.com/file/544D677F5D4E4F17B4122FBD60DB82B3
     * https://ai.baidu.com/file/470B3ACCA3FE43788B5A963BF0B625F3
     * 下载
     */
//    private static String utterance() {
//        // 请求URL
//        String talkUrl = "https://aip.baidubce.com/rpc/2.0/unit/service/chat";
//        try {
//            // 请求参数
//            String params = "{\"log_id\":\"UNITTEST_10000\","//客户端生成的唯一id
//            		+ "\"version\":\"2.0\","  //版本号
//            		+ "\"service_id\":\"S18634\"," //机器人ID，标明该session由哪个机器人产生。
//            		+ "\"session_id\":\"\"," //session本身的ID
//            		+ "\"request\":{\"query\":\"中国的面积多大	\","
//            		+ "\"user_id\":\"88888\"}"	//技能对话的用户id
//            		+ "\"dialog_state\":{\"contexts\":{\"SYS_REMEMBERED_SKILLS\":[\"1057\"]}}}";//机器人对话状态。希望在多技能对话过程中贯穿的全局性上下文
//            
//            String accessToken = "24.df831e26911ddfed0313ad1709961e80.2592000.1561273226.282335-16027515";
//            String result = HttpUtil.post(talkUrl, accessToken, "application/json", params);
//            return result;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
    
	
	public static String utterance(String text) {
        // 请求URL
        String talkUrl = "https://aip.baidubce.com/rpc/2.0/unit/bot/chat";
    	String say = "我还没听明白～";
        try {
            // 请求参数
            String params = "{\"bot_session\":\"\","
            		+ "\"log_id\":\"7758521\","
            		+ "\"request\":{\"bernard_level\":1,"
            		+ "\"client_session\":\"{\\\"client_results\\\":\\\"\\\", "
            		+ "\\\"candidate_options\\\":[]}\","
            		+ "\"query\":\""+ text+ "\","
            		+ "\"query_info\":{\"asr_candidates\":[],"
            		+ "\"source\":\"KEYBOARD\",\"type\":\"TEXT\"},"
            		+ "\"updates\":\"\",\"user_id\":\"88888\"},"
            		+ "\"bot_id\":\"56554\","
            		+ "\"version\":\"2.0\"}";
            String accessToken = "";
            String result1 = HttpUtil.post(talkUrl, accessToken, "application/json", params);
        	JSON jsonData = JSON.parseObject(result1);
			// json转java对象
        	UtteranceBean utteranceBean = JSONObject.toJavaObject(jsonData, UtteranceBean.class);
        	if(utteranceBean.getError_code() == 0){
        		Result result = utteranceBean.getResult();
        		if(result.getResponse().getStatus() == 0){
//        			return result.getResponse();
        			List<Action_list> list= result.getResponse().getAction_list();
        			for (Action_list action_list : list) {
        				say = action_list.getSay();
					}
        			System.out.println("机器人回答："+say);
        			return say;
        		}
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
		return say;
    }
    
    public static void main(String[] args) {
        utterance("珠穆朗玛峰有多高");
    }
}
