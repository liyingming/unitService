package com.lym.common.test;

import org.springframework.stereotype.Controller;  
import org.springframework.ui.ModelMap;  
import org.springframework.web.bind.annotation.RequestMapping;  
/**
 * 测试 项目启动是否正常
 * @author liyingming
 * @Jautodoc 荒墨丶迷失  百度AI社区版主
 */
@Controller  
@RequestMapping(value="/HelloWorld") 
public class HelloWorldController {  
   
	//测试项目启动是否可用 (如果可以访问成功 说明项目启动正常)
	//访问地址 http://localhost:8080/Global_AI_1.0/hello.do 
	@RequestMapping(value="hello.do")  
    public String printWelcome(ModelMap model) {  
        model.addAttribute("message", "Spring 3 MVC Hello World");  
        return "/test/hello";  
    }  
}
