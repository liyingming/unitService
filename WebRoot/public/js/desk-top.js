	//工作台顶部
	var deskTopdom ='<div class="desk-logo">'+
						'<img src="img/public/logo.png" alt="">'+
					'</div>'+
					'<div class=" cityCouponArea">'+
						'<div class="deskCity">'+
							'<div class="city-title">'+
								'<span class="city-title-cancel">取消</span>'+
								'<span>请选择</span>'+
								'<span class="city-title-ensure">确定</span>'+
								// '<img src="img/city-close.png" alt="">'+
							'</div>'+
							'<div class="city-select">'+
								'<div class="city-province" cityId="1">'+
									'<div class="city-circle">'+
										'<div class="city-line"></div>'+
									'</div>'+
									'<div class="city-province-text">'+
										'<span class="text"></span>'+
										'<span class="city-remark">请选择省份</span>'+
										'<span class="city-click none">（点击切换身份/地区）</span>'+
									'</div>'+
								'</div>'+
								'<div class="city-level" cityId="2">'+
									'<div class="city-circle">'+
										'<div class="city-line"></div>'+
									'</div>'+
									'<div class="city-province-text">'+
										'<span class="text"></span>'+
										'<span class="city-remark">请选择城市</span>'+
										'<span class="city-click none">（点击切换城市）</span>'+
									'</div>'+
								'</div>'+
								'<div class="city-class" cityId="3">'+
									'<div class="city-circle">'+
										'<div class="city-line"></div>'+
									'</div>'+
									'<div class="city-province-text">'+
										'<span class="text"></span>'+
										'<span class="city-remark">请选择县</span>'+
										'<span class="city-click none">（点击切换区/县）</span>'+
									'</div>'+
								'</div>'+
								'<div class="city-street" cityId="4">'+
									'<div class="city-circle">'+
									'</div>'+
									'<div class="city-province-text">'+
										'<span class="text"></span>'+
										'<span class="city-remark">请选择街道</span>'+
										'<span class="city-click none">（点击切换街道）</span>'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="city-rule"></div>'+
							'<div class="city-content">'+
								'<div class="select-text">请选择省份</div>'+
								'<div class="city-select-wrap">'+
									'<div class="city-select-content" cityId="1">'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
						'<div class="desk-area">'+
							'<img align="center" src="img/public/location.png" alt="">'+
							'<span class="desk-city"><span class="province-text">上海</span><span class="city-text"></span><span class="county-text"></span><span class="street-text"></span></span>'+
							'<img align="center" src="img/public/more.png" alt="">'+
						'</div>'+
					'<div class="desk-function">'+
						'<div>'+
							'<div>'+
								'<img src="img/public/manual.png" alt="">'+
							'</div>'+
							'<div class="desk-fun-text">呼叫人工</div>'+
						'</div>'+
						'<div>'+
							'<div>'+
								'<img src="img/public/video.png" alt="">'+
							'</div>'+
							'<div class="desk-fun-text">视频咨询</div>'+
						'</div>'+
						'<div>'+
							'<div>'+
								'<img src="img/public/voice.png" alt="">'+
							'</div>'+
							'<div class="desk-fun-text">语音助手</div>'+
						'</div>'+
						'<div>'+
							'<div>'+
								'<img src="img/public/help.png" alt="">'+
							'</div>'+
							'<div class="desk-fun-text">帮助</div>'+
						'</div>'+
					'</div>'+
					'<div class="desk-login">'+
						'<div class="desk-restore">'+
							'<img src="img/public/index.png" alt="">'+
						'</div>'+
						'<div class="dask-detail">'+
							'<div class="desk-img">'+
								'<img src="img/public/22.png" alt="">'+
							'</div>'+
							'<div class="desk-info">'+
								'<div class="desk-name">沙扬娜拉</div>'+
								'<div class="desk-exit">退出</div>'+
							'</div>'+
						'</div>'+
					'</div>';
	$(".desk-top").append(deskTopdom);
	//工作台尾部
	var deskBottomDom = '<div class="desk-type"><span class="desk-device">设备编号：ZF001 </span><span class="desk-version">版本号：v2.0.0.1</span></div>'+
						'<div class="desk-time">倒计时<span class="desk-count-time"><span class="desk-count">90</span>秒</span>后退出系统</div>';
	$(".desk-bottom").append(deskBottomDom);


	function animate(id){
	 	$(id).click(function(){
	 		$(this).addClass('animated bounceIn');
	 		setTimeout(function(){
	 		   $(id).removeClass('animated bounceIn');	
	 		},500)
	 	})	            	
    }
    animate(".dialog-print-btn img,.phone-mini,.desk-function >div,.desk-prev,.desk-next,.desk-print,.desk-restore,.desk-exit,.desk-area,.desk-cancel,.ensure,.cancel,.calling-code,.calling-id,.js-page-prev,.js-page-next");

    $(".dialog-close").click(function(){
		$(".coupon").addClass("none");
	})
	$(".look-close").click(function(){
		$(".verify-coupon,.coupon .verify-look,.video-depart,.video-work,.video-phone,.consult-phone,.consult-video,.phone-mini").addClass("none");
	})

	// 视频咨询 验证方式
	var queryWay = 	'<div class="coupon verify-coupon video-way">'+
						'<div class="verify-look">'+
							'<div class="look-title">请选择验证方式</div>'+
							'<div class="look-wrap">'+
								'<div class="way-content centerX">'+
									'<div class="way-item scale">'+
										'<div class="centerXY">'+
											'<img align="center" src="img/query-face.png" alt="">'+
										'</div>'+
										'<div class="way-title text">人脸认证</div>'+
									'</div>'+
									'<div class="way-item space">'+
										'<div class="centerXY">'+
											'<img align="center" src="img/query-finger.png" alt="">'+
										'</div>'+
										'<div class="way-title text">指纹认证</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="look-close">'+
								'<img src="img/close.png" alt="">'+
							'</div>'+
						'</div>'+
					'</div>';


  	// 视频咨询 部门
  	var departHtml ='<div class="video-depart none">'+
						'<div class="video-letter flex">'+
							'<div class="flex1 video-letter-select">全部</div>'+
							'<div class="flex1">A-I</div>'+
							'<div class="flex1">J-R</div>'+
							'<div class="flex1">S-Z</div>'+
						'</div>'+
						'<div class="video-remark">'+
							'<img align="center" src="img/video-remark.png" alt="">'+
							'<span>温馨提示：部门检索默认显示全部部门，按首字母顺序依次排列。</span>'+
						'</div>'+
						'<div class="swiper-video">'+
							'<div class="swiper-pagination-video"></div>'+
							'<div class="swiper-wrapper">'+
								'<div class="swiper-slide">'+
									'<div class="video-depart-content">'+
										'<div class="video-item video-select">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
									'</div>'+
								'</div>'+
								'<div class="swiper-slide">'+
									'<div class="video-depart-content">'+
										'<div class="video-item video-select">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
										'<div class="video-item">教育局</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+	
					'</div>';
	// 视频咨询 工作人员
	var workHtml = 
					'<div class="video-work none">'+
						'<div class="swiper-work">'+
							'<div class="swiper-pagination-work"></div>'+
							'<div class="swiper-wrapper">'+
								'<div class="swiper-slide">'+
									'<div class="worker-item">'+
										'<div class="worker-header">'+
											'<div class="worker-name">衡山路</div>'+
											'<div class="worker-unit">煤气灯Livehouse</div>'+
										'</div>'+
										'<div class="worker-detail">'+
											'<div class="worker-title">号码</div>'+
											'<div class="worker-tel">89757</div>'+
											'<div class="worker-title">备注</div>'+
											'<div class="worker-remark">我只是小酒馆里的歌手</div>'+
										'</div>'+
										'<div class="worker-btn">电话咨询</div>'+
									'</div>'+
									'<div class="worker-item">'+
										'<div class="worker-header">'+
											'<div class="worker-name">衡山路</div>'+
											'<div class="worker-unit">煤气灯Livehouse</div>'+
										'</div>'+
										'<div class="worker-detail">'+
											'<div class="worker-title">号码</div>'+
											'<div class="worker-tel">89757</div>'+
											'<div class="worker-title">备注</div>'+
											'<div class="worker-remark">我只是小酒馆里的歌手</div>'+
										'</div>'+
										'<div class="worker-btn">电话咨询</div>'+
									'</div>'+
									'<div class="worker-item">'+
										'<div class="worker-header">'+
											'<div class="worker-name">衡山路</div>'+
											'<div class="worker-unit">煤气灯Livehouse</div>'+
										'</div>'+
										'<div class="worker-detail">'+
											'<div class="worker-title">号码</div>'+
											'<div class="worker-tel">89757</div>'+
											'<div class="worker-title">备注</div>'+
											'<div class="worker-remark">我只是小酒馆里的歌手</div>'+
										'</div>'+
										'<div class="worker-btn">电话咨询</div>'+
									'</div>'+
									'<div class="worker-item">'+
										'<div class="worker-header">'+
											'<div class="worker-name">衡山路</div>'+
											'<div class="worker-unit">煤气灯Livehouse</div>'+
										'</div>'+
										'<div class="worker-detail">'+
											'<div class="worker-title">号码</div>'+
											'<div class="worker-tel">89757</div>'+
											'<div class="worker-title">备注</div>'+
											'<div class="worker-remark">我只是小酒馆里的歌手</div>'+
										'</div>'+
										'<div class="worker-btn">电话咨询</div>'+
									'</div>'+
								'</div>'+
								'<div class="swiper-slide">'+
									'<div class="worker-item">'+
										'<div class="worker-header">'+
											'<div class="worker-name">衡山路</div>'+
											'<div class="worker-unit">煤气灯Livehouse</div>'+
										'</div>'+
										'<div class="worker-detail">'+
											'<div class="worker-title">号码</div>'+
											'<div class="worker-tel">89757</div>'+
											'<div class="worker-title">备注</div>'+
											'<div class="worker-remark">我只是小酒馆里的歌手</div>'+
										'</div>'+
										'<div class="worker-btn">电话咨询</div>'+
									'</div>'+
									'<div class="worker-item">'+
										'<div class="worker-header">'+
											'<div class="worker-name">衡山路</div>'+
											'<div class="worker-unit">煤气灯Livehouse</div>'+
										'</div>'+
										'<div class="worker-detail">'+
											'<div class="worker-title">号码</div>'+
											'<div class="worker-tel">89757</div>'+
											'<div class="worker-title">备注</div>'+
											'<div class="worker-remark">我只是小酒馆里的歌手</div>'+
										'</div>'+
										'<div class="worker-btn">电话咨询</div>'+
									'</div>'+
									'<div class="worker-item">'+
										'<div class="worker-header">'+
											'<div class="worker-name">衡山路</div>'+
											'<div class="worker-unit">煤气灯Livehouse</div>'+
										'</div>'+
										'<div class="worker-detail">'+
											'<div class="worker-title">号码</div>'+
											'<div class="worker-tel">89757</div>'+
											'<div class="worker-title">备注</div>'+
											'<div class="worker-remark">我只是小酒馆里的歌手</div>'+
										'</div>'+
										'<div class="worker-btn">电话咨询</div>'+
									'</div>'+
									'<div class="worker-item">'+
										'<div class="worker-header">'+
											'<div class="worker-name">衡山路</div>'+
											'<div class="worker-unit">煤气灯Livehouse</div>'+
										'</div>'+
										'<div class="worker-detail">'+
											'<div class="worker-title">号码</div>'+
											'<div class="worker-tel">89757</div>'+
											'<div class="worker-title">备注</div>'+
											'<div class="worker-remark">我只是小酒馆里的歌手</div>'+
										'</div>'+
										'<div class="worker-btn">电话咨询</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="swiper-work-next">'+
						    '</div>'+
						    '<div class="swiper-work-prev">'+
						   ' </div>'+
						'</div>'+
						'<div class="desk-btn centerXY">'+
							'<div class="desk-cancel desk-step">上一步</div>'+
							'<div class="desk-next">已知号码，点击拨号</div>'+
						'</div>'+
					'</div>';
	// 视频咨询 拨打电话
	var phoneHtml = '<div class="video-phone flex none">'+
						'<div class="phone-wrap">'+
							'<div class="phone-text">手机号码：</div>'+
							'<div class="phone-input">'+
								'<input class="input_control" type="text" maxlength="11" placeholder="请输入电话号码" readonly="readonly"  onkeyup="this.value=this.value.replace(/\D/g,'+ ')" onafterpaste="this.value=this.value.replace(/\D/g,'+ ')">'+
							'</div>'+
							'<div class="desk-btn">'+
								'<div class="desk-next">拨打</div>'+
								'<div class="desk-cancel desk-step">返回上一步</div>'+
							'</div>'+
						'</div>'+
						'<div class="phone-key">'+
							'<div class="num">1</div>'+
							'<div class="num">2</div>'+
							'<div class="num">3</div>'+
							'<div class="num">4</div>'+
							'<div class="num">5</div>'+
							'<div class="num">6</div>'+
							'<div class="num">7</div>'+
							'<div class="num">8</div>'+
							'<div class="num">9</div>'+
							'<div class="delete">清除</div>'+
							'<div class="num">0</div>'+
							'<div class="close"><img src="img/backclose.png" alt=""></div>'+
						'</div>'+
					'</div>';
					
	// 电话咨询
	var telephoneHtml = '<div class="consult-phone none">'+
							'<div class="consult-header">'+
								'<div  class="consult-min"><img align="center" src="img/consult-min.png" alt=""><span>最小化</span></div>'+
								'<div class="consult-text">电话咨询</div>'+
								'<div class="consult-close"><img align="center" src="img/consult-close.png" alt=""></div>'+
							'</div>'+
							'<div class="consult-wrap">'+
								'<div class="consult-remark">电话咨询中，我们将竭诚为您服务！</div>'+
								'<div class="consult-btn centerX">'+
									'<div class="consult-video-btn">'+
										'<div>'+
											'<img align="center" src="img/consult-video.png" alt="">'+
										'</div>'+
										'<div class="consult-btn-text">切换视频咨询</div>'+
									'</div>'+
									'<div class="consult-over">'+
										'<div>'+
											'<img align="center" src="img/consult-over.png" alt="">'+
										'</div>'+
										'<div class="consult-btn-text">结束咨询</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>';
	// 视频咨询
	var videoHtml = '<div class="consult-video none">'+
						'<div class="consult-header">'+
							'<div  class="consult-min"><img align="center" src="img/consult-min.png" alt=""><span>最小化</span></div>'+
							'<div class="consult-text">视频咨询</div>'+
							'<div class="consult-close">结束咨询</div>'+
						'</div>'+
						'<div class="consult-wrap">'+
						'</div>'+
					'</div>';
					

  	// 视频咨询弹窗
  	var videoCouponHtml = '<div class="coupon   verify-coupon videoConsultation none">'+
  						// 选择部门  工作人员
						'<div class="verify-look none">'+
							'<div class="look-title">请选择部门</div>'+
							'<div class="look-wrap">'+ departHtml + workHtml + phoneHtml +'</div>'+
							'<div class="look-close">'+
								'<img src="img/close.png" alt="">'+
							'</div>'+
						'</div>'+
						telephoneHtml + videoHtml +
					'</div>';

	$(".desk-wrap").append(videoCouponHtml);

	var videoMini = '<div class="phone-mini none">'+
						'<div class="mini-icon">'+
							'<img src="img/phone-icon.png" alt="">'+
						'</div>'+
						'<div class="mini-text">视频咨询</div>'+
					'</div>';
	$(".desk-wrap").append(videoMini);				

  	// 统一弹窗
  	var dialogCoupon = {
		alert:function(text,remark){
			if(text == undefined){
				text = '';
			}
			if(remark == undefined){
				remark = '';
			}

			var alertHtml = '<div class="coupon dialog-end ">'+
								'<div class="dialog-end-content">'+
									'<div class="dialog-smile">'+
										'<img src="img/dialog-smile.png" alt="">'+	
									'</div>'+
									'<div class="dialog-text"><span>“</span>'+ text +'<span>”</span></div>'+
									'<div class="dialog-remark">'+ remark +'</div>'+
									'<div class="dialog-btn centerBetween">'+
										'<div class="ensure">确定</div>'+
										'<div class="cancel">取消</div>'+
									'</div>'+
									'<img class="dialog-close" src="img/close.png" alt="">'	+
								'</div>'+
							'</div>';
			$('.desk-wrap').append(alertHtml);
			$(".dialog-close,.dialog-end .ensure,.dialog-end .cancel").click(function(){
				$(".dialog-end").remove();
			})
		},
		close:function(){
			$(".dialog-end").remove();
		}
	}

	var loadHTml = '<div class="loadCanvas none"></div>';
	$(".desk-wrap").append(loadHTml);
	// 加载中  使用时页面需引入loadCanvas.js
	var loading = {
		alert:function(html){
			$(".load-text").html(html);
			$(".loadCanvas").removeClass("none");		
		},
		close:function(){
			 $(".loadCanvas").addClass("none");
		}
	}


	// 通知弹窗
	var informCoupon = {
		open:function(title,date,content){
			if(title == undefined){
				title = '';
			}
			if(date == undefined){
				date = '';
			}
			if(content == undefined){
				content = '';
			}
			var informHtml ='<div class="coupon inform-coupon">'+
								'<div class="verify-look">'+
									'<div class="look-title">通知</div>'+
									'<div class="inform-title">'+ title +'</div>'+
									'<div class="inform-date">'+ date +'</div>'+
									'<div class="inform-wrap"><div class="inform-content">'+
										content +
									'</div></div>'+
									'<div class="look-close">'+
										'<img src="img/close.png" alt="">'+
									'</div>'+
								'</div>'+
							'</div>';
			$(".desk-wrap").append(informHtml);
			var informWrap = new scrollbot(".inform-wrap");
		},
		close:function(){
			$(".inform-coupon").remove();
		}
	}
	var title = '2018年上海徐汇住房保障和房屋管理局派遣制人员招聘4人公告';
	var date = '发布时间：2018年08月20日';
	var contentHtml = '<div>不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。不可抗力：《合同法》第117条规定，因不可抗力不能履行合同的，根据不可抗力的影响，部分或者全部免除责任，但法律另有规定的除外。</div>';
	// informCoupon.open(title,date,contentHtml);

	$(".desk-restore").click(function(){
		location.href = 'index.html';
	})
	$(".desk-function >div:nth-child(3)").click(function(){
		location.href = '语音搜索-首页.html';
	})

	// 呼叫工作人员
	var phoneWorker = {
		
		open:function(){
			var phoneHtml = '<div class="coupon  phone-worker">'+
								'<div class="verify-look">'+
									'<div class="look-title">呼叫工作人员</div>'+
									'<div class="phone-worker-wrap">'+
										'<div class="text">小卓正在为您呼叫工作人员，请您耐心等待...</div>'+
										'<div class="phone-robot">'+
											'<img src="img/robot-coupon.png" alt="">'+
										'</div>'+
										'<div class="desk-btn centerXY">'+
											'<div class="desk-next">结束呼叫</div>'+
										'</div>'+
									'</div>'+
									'<div class="phone-worker-close">'+
										'<img src="img/close.png" alt="">'+
									'</div>'+
								'</div>'+
							'</div>';
			$(".desk-wrap").append(phoneHtml);
		},
		close:function(){
			$(".phone-worker").remove();
		}
	}


	// 视频咨询 指纹认证
	var fingerCoupon = {
		// 打开
		open:function(){
			var fingerHtml ='<div class="coupon  verify-coupon finger-coupon">'+
						'<div class="verify-look">'+
							'<div class="look-title">指纹认证</div>'+
							'<div class="phone-worker-wrap">'+
								'<div class="text">请在图示位置，进行指纹识别，系统将自动识别您的身份</div>'+
								'<div class="phone-robot">'+
									'<img src="img/finger.png" alt="">'+
								'</div>'+
								'<div class="desk-btn centerXY">'+
									'<div class="desk-next">上一步</div>'+
								'</div>'+
							'</div>'+
							'<div class="phone-worker-close">'+
								'<img onclick="fingerCoupon.close()" src="img/close.png" alt="">'+
							'</div>'+
						'</div>'+
					'</div>';
			$(".desk-wrap").append(fingerHtml);
		},
		// 关闭
		close:function(){
			$(".finger-coupon").remove();
		}

	}
	
	// 视频咨询 人脸认证
	var faceCoupon = {
		// 打开
		open:function(){
			var faceHtml = 	'<div class="coupon  verify-coupon  face-coupon">'+
								'<div class="verify-look">'+
									'<div class="look-title">人脸认证</div>'+
									'<div class="phone-worker-wrap">'+
										'<div class="text">正在进行身份验证，请看屏幕上方的摄像头</div>'+
										'<div class="phone-robot">'+
										'</div>'+
										'<div class="desk-btn centerXY">'+
											'<div class="desk-next">上一步</div>'+
										'</div>'+
									'</div>'+
									'<div class="phone-worker-close" onclick="faceCoupon.close()">'+
										'<img src="img/close.png" alt="">'+
									'</div>'+
								'</div>'+
							'</div>';
			$(".desk-wrap").append(faceHtml);
	
		},
		// 关闭
		close:function(){
			$(".face-coupon").remove();
		}

	}


	var videoIdenfity = {
		// 打开  
		open:function(scale,space){
			$(".desk-wrap").append(queryWay);
			$(".video-way .scale").click(function(){
				scale();  //选择人脸认证事件
			});
			$(".video-way .scale").click(function(){
				space();	//选择指纹认证事件
			});
			$(".video-way .look-close").click(function(){
				videoIdenfity.close();	//选择指纹认证事件
			});
		},
		// 关闭
		close:function(){
			$(".video-way").remove();
		}
		
	}

	
























    