	// 选择部门
	var swiperVideo= new Swiper('.swiper-video', {
		pagination: {
			el: '.swiper-pagination-video',
		},
    }); 

	// 选择部门 选择切换
	$(".video-letter >div").click(function(){
		$(".video-letter >div").removeClass("video-letter-select");
		$(this).addClass("video-letter-select");
		swiperVideo.update();
	})

	$(".video-depart-content .video-item").click(function(){
		$(".video-depart-content .video-item").removeClass("video-select");
		$(this).addClass("video-select");
	})

	// 选择工作人员
    var swiperWorker = new Swiper('.swiper-work', {
		pagination: {
			el: '.swiper-pagination-work',
		},
		navigation: {
		  nextEl: '.swiper-work-next',
		  prevEl: '.swiper-work-prev',
		},
    });	
    $(".swiper-work-next").append("<i class='iconfont icon-left-copy-right'></i>");
    $(".swiper-work-prev").append("<i class='iconfont icon-left--copy-left'></i>");
    var input = $('.input_control');
    //软键盘填写金额
    $('.phone-key .num').click(function(){
        var numText = $(this).html();
        var numTexts = input.val();
        input.val(numTexts+numText);
        if (input.val().length>11)
        {
            input.val( input.val().substring (0, 11));
        }
    });
    //清除
    $('.phone-key .delete').click(function(){
        input.val('');
    });
    //后退
    $('.phone-key .close').click(function(){
        var str=input.val();
        input.val(str.substring(0,str.length-1));
    });

    // 点击咨询
    $(".desk-function >div:nth-child(1),.desk-function >div:nth-child(2)").click(function(){
    	$(".videoConsultation,.consult-phone,consult-video").css({
    		"animation": "none 0 none 0 none ",
    	})
    	$(".look-title").html("请选择部门");
    	$(".videoConsultation").removeClass("none verify-coupon-mini");
    	$(".verify-look,.video-depart").removeClass("none");
        $(".video-work,.video-phone,.consult-phone,.consult-video").addClass("none");
    	swiperVideo.update();
    })
    // 点击部门
    $(".video-depart .video-item").click(function(){
    	$(".look-title").html("请选择工作人员");
    	$(".video-depart").addClass("none");
    	$(".video-work").removeClass("none");
    	swiperWorker.update();
    })
    // 点击选择工作人员 上一步
    $(".video-work .desk-cancel").click(function(){
    	$(".look-title").html("请选择部门");
    	$(".video-work").addClass("none");
    	$(".video-depart").removeClass("none");
    	swiperVideo.update();
    })

    // 点击电话咨询
    $(".worker-btn").click(function(){
    	$(".look-title").html("拨打工作人员电话");
    	$(".video-work").addClass("none");
    	$(".video-phone").removeClass("none");
    })
    $(".video-work .desk-next").click(function(){
    	$(".look-title").html("拨打工作人员电话");
    	$(".video-work").addClass("none");
    	$(".video-phone").removeClass("none");
    })

    // 点击拨打电话 上一步
    $(".video-phone .desk-cancel").click(function(){
    	$(".look-title").html("请选择工作人员");
    	$(".video-phone").addClass("none");
    	$(".video-work").removeClass("none");
    	swiperWorker.update();
    })
    // 点击拨号
    $(".video-phone .desk-next").click(function(){
    	$(".video-phone").addClass("none");
    	$(".verify-look").addClass("none");
    	$(".consult-phone").removeClass("none");
    })


    // 电话咨询 结束咨询
    $(".consult-over").click(function(){
    	$(".videoConsultation").addClass("none");
    	$(".consult-phone").addClass("none");
    	$(".phone-mini").addClass("none");
    })
    // 切换视频咨询
    $(".consult-video-btn").click(function(){
    	$(".consult-phone").addClass("none");
    	$(".consult-video").removeClass("none");
    })

    // 视频咨询  结束咨询
    $(".consult-video .consult-close").click(function(){
		$(".consult-video").addClass("none");
		$(".phone-mini").addClass("none");
    })
   	// 关闭视频、电话咨询
   	$(".consult-close").click(function(){
   		$(".consult-phone,.consult-video").addClass("none");
   		$(".videoConsultation").addClass("none");
   	})
    // 最小化
    $(".consult-min").click(function(){
    	$(".consult-phone,.consult-video").addClass("none");
    	$(".consult-phone,.consult-video").addClass("none");
    	$(".videoConsultation").addClass("none");
    	$(".phone-mini").removeClass("none");
    	$(".phone-mini").css({
    		"animation": "phoneMiniMax 2s ease",
    	})
    })
    // 最小化 复原
    $(".phone-mini").click(function(){
    	$(".consult-phone").removeClass("none");
    	$(".verify-look,.video-depart,.video-work").addClass("none");
    	$(".videoConsultation").removeClass("none");
    	$(".phone-mini").addClass("none");
    	// $(".consult-phone").css({
    	// 	"animation": "videoPhoneMax 0.1s ease"
    	// })
    	// $(".videoConsultation").css({
    	// 	"animation": "videoMax 1s ease",
    	// })
    })

    $(".videoConsultation .look-close").click(function(){
        $(".videoConsultation").addClass("none");
    })
