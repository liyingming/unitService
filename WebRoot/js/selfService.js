
	$(".single .select-item").click(function(){
		var isSelect = $(this).hasClass("selected");
		if(isSelect){
			$(this).removeClass("selected");
			$(this).find("input").prop('checked', false);
		}else{
			$(this).parent().find(".select-item").removeClass("selected");
			$(this).parent().find(".select-item").find("input").prop('checked', false);
			$(this).find("input").prop('checked', true);
			$(this).addClass("selected");
		}
	})
	$(".multiSelect .select-item").click(function(){
		var isSelect = $(this).hasClass("selected");
		if(isSelect){
			$(this).removeClass("selected");
			$(this).find("input").prop('checked', false);
		}else{
			$(this).addClass("selected");
			$(this).find("input").prop('checked', false);
		}
	})

	