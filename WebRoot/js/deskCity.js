	document.write("<script type='text/javascript' src='js/loadCanvas.js'></script>"); 
	var cityWrap; 
	window.onload = function(){
		cityWrap = new scrollbot(".city-select-wrap");
		province();
	}
	$(".desk-area").click(function(){
		$(".cityCouponArea").addClass("cityCoupon");
		cityWrap.refresh();	
	})
	$(".city-title img").click(function(){
		$(".desk-area").parent().removeClass("cityCoupon");
	})

	var remarkHtml = {
		'1':"请选择省份",
		'2':"请选择城市",
		'3':"请选择区",
		'4':"请选择街道"
	}

	$(".city-select >div").click(function(){
		var startjudge = $(".city-province").attr("province");
		if(startjudge == undefined && $(this).attr("cityId") != '1'){
			dialogCoupon.alert("请先选择省份！");
			return false;
		}
		var cityId = parseInt( $(this).attr("cityId"));
		$(".city-select-content").attr("cityId",cityId);
		$(".city-select >div").removeClass("city-select-click");
		$(this).addClass("city-select-click");
		var provinceID = parseInt( $(".city-province").attr("province"));
		var city = $(this).attr("city");
		var cityId = parseInt($(this).attr("cityid"));
		if(cityId == 1){
			province();
		}else if(cityId == 2){
			cityFun(provinceID);
		}else if( cityId == 3){
			countyFun(provinceID,city);
		}

		var remarkText = remarkHtml[$('.city-select-content').attr("cityid")];
		$(".select-text").html(remarkText);

	})
	
	var content =  function(obj){	
		$(obj).addClass("city-choose");
		var chooseVal = $(obj).html();
		var cityId = $(obj).parent().attr("cityId");
		
		var clickDom = $(".city-select >div[cityId='"+ cityId +"']");
		clickDom.prevAll().addClass("select").removeClass("none city-select-click");
		clickDom.addClass("select").removeClass("none city-select-click");
		clickDom.nextAll().addClass("none").removeClass("select");
		clickDom.next().removeClass("none").removeClass("select").addClass("city-select-click");
		clickDom.next().find(".text").html("");
		clickDom.next().find(".city-remark").removeClass("none");
		clickDom.next().find(".city-click").addClass("none");
		clickDom.find(".city-remark").addClass("none");
		clickDom.find(".city-click").removeClass("none");
		clickDom.find(".text").html(chooseVal);
		clickDom.next().removeClass("none");
		if(parseInt(cityId) == 4){
			return false;
		}
		if(parseInt(cityId)  < 4){
			cityId++;
			$(".city-select-content").attr("cityId",cityId);	
		}
		var province = $(obj).attr("province");
		var city = $(obj).attr("city");
		var county = $(obj).attr("county");
		if($(obj).attr("county") != undefined){
			
			// 获取县/区级地名
			$(".city-province,.city-level,.city-class,.city-street").attr("county",county);
			streetFun(province,city,county);
		}else if($(obj).attr("city") != undefined){
		
			// 获取县/区级地名
			$(".city-province,.city-level,.city-class,.city-street").attr("city",city);
			countyFun(province,city);
		
		}else if($(obj).attr("province") != undefined){	
		
			// 获取市级地名
			$(".city-province,.city-level,.city-class,.city-street").attr("province",province);
			cityFun(province);
		}
	}
	var cityData = dsy['Items'];
	function province(){
		$('.city-select-content >div').remove();
		var provinceArray = cityData[0];
		var s = '';
		provinceArray.forEach(function(data,index){
			s += '<div province="'+ index +'" onclick="content(this)">'+ data +'</div>';
		})
		$('.city-select-content').append(s);
	
		cityWrap.refresh();
		var remarkText = remarkHtml[$('.city-select-content').attr("cityid")];
		$(".select-text").html(remarkText);
		
		// 删除历史数据
		$(".city-province,.city-level,.city-class,.city-street").removeAttr("city");
		$(".city-province,.city-level,.city-class,.city-street").removeAttr("county");
		$(".city-province,.city-level,.city-class,.city-street").removeAttr("street");
	}
	
	// 获取市级地名
	function cityFun(id){
		$('.city-select-content >div').remove();
		var cityArray = cityData["0_" + id];
		if(cityArray == undefined){
			return;
		}
		var s = '';
		cityArray.forEach(function(data,index){
			s += '<div province="'+ id +'" city="'+ index +'" onclick="content(this)">'+ data +'</div>';
		})
		$('.city-select-content').append(s);

		cityWrap.refresh();
		var remarkText = remarkHtml[$('.city-select-content').attr("cityid")];
		$(".select-text").html(remarkText);
		// 删除历史数据
		$(".city-province,.city-level,.city-class,.city-street").removeAttr("county");
		$(".city-province,.city-level,.city-class,.city-street").removeAttr("street");
	}

	// 获取县级地名
	function countyFun(province,city){
		$('.city-select-content >div').remove();
		var cityArray = cityData["0_" + province + '_'+ city];
		if(cityArray == undefined){
			return;
		}
		var s = '';
		cityArray.forEach(function(data,index){
			s += '<div  province="'+ province +'" city="'+ city +'" county="'+ index +'" onclick="content(this)">'+ data +'</div>';
		})
		$('.city-select-content').append(s);
		
		cityWrap.refresh();
		var remarkText = remarkHtml[$('.city-select-content').attr("cityid")];
		$(".select-text").html(remarkText);
		// 删除历史数据
		$(".city-province,.city-level,.city-class,.city-street").removeAttr("street");
	}

	// 获取街道地名
	function streetFun(province,city,county){
		$('.city-select-content >div').remove();
		var cityArray = cityData["0_" + province + '_'+ city + '_' + county];
		if(cityArray == undefined){
			return;
		}
		var s = '';
		s += '<div  province="'+ province +'" city="'+ city +'" county="'+ county +'"  onclick="choose(this)">暂不选择</div>';
		cityArray.forEach(function(data,index){
			s += '<div  province="'+ province +'" city="'+ city +'" county="'+ county +'" street="'+ index +'" onclick="choose(this)">'+ data +'</div>';
		})
		$('.city-select-content').append(s);
		cityWrap.refresh();
		var remarkText = remarkHtml[$('.city-select-content').attr("cityid")];
		$(".select-text").html(remarkText);
	}

	function choose(obj){
		var chooseVal = $(obj).html();
		$(".city-street .text").html(chooseVal);
		if(chooseVal.indexOf("暂不选择") == -1){
			$(".city-select-content >div").removeClass("city-choose");
			$(obj).addClass("city-choose");
			$(".city-street").addClass("select");
			$(".city-street .city-remark").addClass("none");
			$(".city-street .city-click").removeClass("none");
			var street = $(obj).attr("street");
			$(".city-province,.city-level,.city-class,.city-street").attr("street",street);
		}
	}

	$(".city-title .city-title-cancel").click(function(){
		$(".cityCouponArea").removeClass("cityCoupon");
	})
	$(".city-title .city-title-ensure").click(function(){
		addSelectDetail();
		$(".cityCouponArea").removeClass("cityCoupon");
	})


	var addSelectDetail = function(){
		$(".province-text").html($(".city-province .text").html());
		$(".city-text").html($(".city-level .text").html());
		$(".county-text").html($(".city-class .text").html());
		$(".street-text").html($(".city-street .text").html());
	}
