function Dsy(){
	this.Items = {};
}
Dsy.prototype.add = function(id,iArray){
	this.Items[id] = iArray;
}
Dsy.prototype.Exists = function(id){
	if(typeof(this.Items[id]) == "undefined") return false;
	return true;
}
var dsy = new Dsy();

dsy.add("0",["北京市","上海市","江苏省"]);
dsy.add("0_0",["北京市"]);
dsy.add("0_0_0",["东城区","西城区"]);
dsy.add("0_0_0_0",["东长安街","后海街道"]);
dsy.add("0_0_0_1",["西长安街","管庄街道"]);
dsy.add("0_1",["上海市"]);
dsy.add("0_1_0",["徐汇区","闵行区"]);
dsy.add("0_1_0_0",["漕宝路街道","徐汇街道"]);
dsy.add("0_1_0_1",["梅陇街道","罗锦路街道"]);
dsy.add("0_2",["南京市","苏州市","淮安市"]);
dsy.add("0_2_0",["玄武区","浦口"]);
dsy.add("0_2_1",["姑苏区","吴中区"]);
dsy.add("0_2_2",["楚州区","淮阴区"]);
dsy.add("0_2_0_0",["玄武街道1","玄武街道2"]);
dsy.add("0_2_0_1",["浦口街道1","浦口街道2"]);
dsy.add("0_2_1_0",["姑苏街道1","姑苏街道2"]);
dsy.add("0_2_1_1",["吴中街道1","吴中街道2"]);
dsy.add("0_2_2_0",["楚州街道1","楚州街道2"]);
dsy.add("0_2_2_1",["淮阴街道1","淮阴街道2"]);
