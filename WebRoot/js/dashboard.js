	// 办件量统计 饼图
    var myChart = echarts.init(document.getElementById('handle-data'));
    var option = {
    	color:['#6077e9','#ce95f1','#8e9ff8'],
	    tooltip: {
	        trigger: 'item',
	        formatter: "{a} <br/>{b}: {c} ({d}%)"
	    },
	    legend: {
	        orient: 'vertical',
	        top:0,
	        right:20,
	        textStyle:{
	        	color:"#757575",
	        	fontSize:14
	        },
	        data:['今日','本周','本月']
	    },
	    series: [
	        {
	            name:'办件量统计',
	            type:'pie',
	            radius: ['50%', '70%'],
	            avoidLabelOverlap: false,
	            label: {
	                normal: {
	                    show: false,
	                    position: 'center'
	                },
	                
	                emphasis: {
	                    show: true,
	                    formatter:'{c}',
	                    textStyle: {
	                        fontSize: '30',
	                        fontWeight: 'bold'
	                    }
	                }
	            },
	            labelLine: {
	                normal: {
	                    show: false
	                }
	            },
	            data:[
	                {value:335, name:'今日'},
	                {value:310, name:'本周'},
	                {value:234, name:'本月'},
	            ]
	        }
	    ]
	};
    myChart.setOption(option);


    // 办件折线图
    var chart = echarts.init(document.getElementById('statistics'));
    var myOption = {
	    tooltip: {
	        trigger: 'axis',
	        axisPointer: {
	            lineStyle: {
	                color: '#57617B'
	            }
	        }
	    },
	    legend: {
	        icon: 'circle',
	        itemWidth: 10,
	        itemHeight: 10,
	        itemGap: 20,
	        data: [ '实人认证', '身份证复印','材料复印'],
	        right: '2%',
	        textStyle: {
	            fontSize: 14,
	            color: '#757575'
	        }
	    },
	    grid: {
	        left: '2%',
	        right: '2.5%',
	        bottom: '3%',
	        containLabel: true
	    },
	    xAxis: [{
	        type: 'category',
	        boundaryGap: false,
	        axisLine: {
	        	show:false,
	            lineStyle: {
	                color: '#57617B'
	            }
	        },
	        axisTick: {
	            show: false
	        },
	        splitLine: {
	        	show:false,
	        },
	        axisLabel: {
		       show: true,
		        textStyle: {
		          color: '#999999',  
		          fontSize : 14  
		        }
		    },
	        data:['04/03', '04/04', '04/05', '04/06', '04/07', '04/08', '04/09']
	    }],
	    yAxis: [{
	        type: 'value',
	        name: null,
	        axisTick: {
	            show: false
	        },
	        axisLine: {
	        	show:false,
	            lineStyle: {
	                color: '#57617B'
	            }
	        },
	        axisLabel: {
	            margin: 10,
	            textStyle: {
	                fontSize: 14,
	                color:'#999999'
	            }
	        },
	        splitLine: {
	        	show:false,
	            lineStyle: {
	                color: '#57617B'
	            }
	        }
	    }],
	    series: [ {
	        name: '实人认证',
	        type: 'line',
	        smooth: true,
	        symbol: 'circle',
	        symbolSize: 5,
	        showSymbol: false,
	        lineStyle: {
	            normal: {
	                width: 2
	            }
	        },
	        areaStyle: {
	            normal: {
	                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
	                    offset: 0,
	                    color: 'rgba(108, 167, 238, 0.3)'
	                }, {
	                    offset: 0.8,
	                    color: 'rgba(108, 167, 238, 0)'
	                }], false),
	                shadowColor: 'rgba(0, 0, 0, 0.1)',
	                shadowBlur: 10
	            }
	        },
	        itemStyle: {
	            normal: {
	                color: '#6ca7ee',
	                borderColor: 'rgba(0,136,212,0.2)',
	                borderWidth: 12

	            }
	        },
	        data: [10, 10, 15, 15, 12, 16, 12]
	    }, {
	        name: '身份证复印',
	        type: 'line',
	        smooth: true,
	        symbol: 'circle',
	        symbolSize: 5,
	        showSymbol: false,
	        lineStyle: {
	            normal: {
	                width: 2
	            }
	        },
	        areaStyle: {
	            normal: {
	                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
	                    offset: 0,
	                    color: 'rgba(27, 230, 181, 0.3)'
	                }, {
	                    offset: 0.8,
	                    color: 'rgba(27, 230, 181, 0)'
	                }], false),
	                shadowColor: '#1be6b5',
	                shadowBlur: 10
	            }
	        },
	        itemStyle: {
	            normal: {
	                color: '#1be6b5',
	                borderColor: 'rgba(219,50,51,0.2)',
	                borderWidth: 12
	            }
	        },
	        data: [20, 18, 12, 15, 12, 11, 14]
	    },{
	        name: '材料复印',
	        type: 'line',
	        smooth: true,
	        symbol: 'circle',
	        symbolSize: 5,
	        showSymbol: false,
	        lineStyle: {
	            normal: {
	                width: 2
	            }
	        },
	        areaStyle: {
	            normal: {
	                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
	                    offset: 0,
	                    color: 'rgba(163, 137, 211, 0.3)'
	                }, {
	                    offset: 0.8,
	                    color: 'rgba(163, 137, 211, 0)'
	                }], false),
	                shadowColor: 'rgba(0, 0, 0, 0.1)',
	                shadowBlur: 10
	            }
	        },
	        itemStyle: {
	            normal: {
	                color: '#a389d3',
	                borderColor: 'rgba(137,189,2,0.27)',
	                borderWidth: 12

	            }
	        },
	        data: [20, 12, 11, 14, 10, 10, 10]
	    } ]
	};
	chart.setOption(myOption);

	// 统计时间选择
	$(".total-time >span").click(function(){
		$(".total-time >span").removeClass("time-select");
		$(this).addClass("time-select");
	})