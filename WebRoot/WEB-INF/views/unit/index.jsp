<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>智能问答</title>
<script src="../public/js/jquery-3.3.1.min.js"></script>
<script src="../js/HZRecorder.js"></script>
<script src="../public/js/desk-top.js"></script>
<script src="../js/scrollbot.js"></script>
<script src="../js/scrollreveal.min.js"></script>
<script src="../js/query.js"></script>
<script src="../js/paging.js"></script>
<script src="../js/swiper4.4.6.min.js"></script>
<script src="../js/video.js"></script>
<link rel="stylesheet" href="../public/css/base.css">
<link rel="stylesheet" href="../css/index_top.css">
<link rel="stylesheet" href="../css/animate.css">
<link rel="stylesheet" href="../css/selfService.css">
<link rel="stylesheet" href="../public/css/desk-top.css">
<link rel="stylesheet" href="../css/query.css">
<link rel="stylesheet" href="../css/paging.css">
<link rel="stylesheet" href="../css/swiper4.4.6.min.css">
<link rel="stylesheet" href="../css/index.css">
<style>
.voice-search {
	position: absolute;
	bottom: -100px;
	margin-bottom: 0;
	left: 50%;
	margin-left: -580px;
}

.voice-btn {
	bottom: -140px;
	left: 1239px;
}
</style>
</head>
<body>
	<div class="desk-wrap">
		<div class="desk-top none"></div>
		<div class="desk-content">
			<div id="wrd" class="wrap_right dialogBox" style="margin-left: 65px;overflow:auto;">
				<div class="dialog_wrap" id="message_box">

					<div class="dialog_item dialog_robot">
						<!-- <div class="time">2018-08-17 11:26:26</div> -->
						<div class="flex">
							<div class="dialog_left">
								<img src="../img/dialog-robot.png" alt="">
							</div>
							<div class="dialog_right">
								<div class="ask">你可以这么问我：</div>
								<div class="dialog_content">
									<div>1.珠穆朗玛峰有多高</div>
									<div>2.中国面积有多大？</div>
									<div>3.哺乳动物的释义？</div>
									<div>4.橘子的营养成分？</div>
									<div>5.五谷是指什么？</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<audio id="aud"></audio>
			</div>
				<div class="hot-search voice-search">
				<input class="hot-input" type="text" id="message" placeholder="请用一句话简要、准确地描述您的问题">
				<img class="hot-search-btn" src="../img/search-send.png" alt="">
			</div>
			<img class="voice-btn" src="../img/voice-icon.png" alt="">
		</div>
	</div>
	<div class="coupon dialog-coupon none">
		<div class="dialog-coupon-content">
			<div class="dialog-icon">
				<img src="../img/dialog-voice.png" alt="">	
				<img class="rotate" src="../img/aperture.png" alt="">
			</div>
			<div class="dialog-text">正在识别你说话</div>
			<div class="dialog-text none">单击结束说话</div>
			<div class="dialog-text none">正在倾听您的讲话</div>
			<img class="dialog-close" src="../img/close.png" alt="">	
		</div>
	</div>
	<div class="coupon dialog-end  none">
		<div class="dialog-end-content">
			<div class="dialog-smile">
				<img src="../img/dialog-smile.png" alt="">	
			</div>
			<div class="dialog-text"><span>“</span>开启语音服务，将结束当前正在办理的事项。<span>”</span></div>
			<div class="dialog-remark">您确定这么操作吗？</div>
			<div class="dialog-btn centerBetween">
				<div class="ensure">确定</div>
				<div class="cancel">取消</div>
			</div>
			<img class="dialog-close" src="../img/close.png" alt="">	
		</div>
	</div>
	<script type="text/javascript">
	
	//这个是初始化语音 当然也可以用 流播放了~
	var audio3 = new Audio("${pageContext.request.contextPath}/voice/huanying.mp3");
	audio3.play();
	
	/* 音频 */
	var recorder;
	var audio = document.querySelector('audio');//用于合成录音
	//开始录音
	function startRecording() {
		HZRecorder.get(function(rec) {
			recorder = rec;
			recorder.start();
		});
	}
	//获取录音
	function obtainRecord() {
		var record = recorder.getBlob();
	};
	//停止录音
	function stopRecord() {
		recorder.stop();
	};

	function playRecord() {
		recorder.play(audio);
	};
		
	//保存录音
	function uploadRecord(){
		//上传录音地址
		recorder.upload("${pageContext.request.contextPath}/unit/saveVocie.do", function (state, e) {
			switch (state) {
                   case 'uploading':
                       break;
                   case 'ok':   
                   	var map = JSON.parse(e.target.response);
                       if (map["success"]) {
                       	var Rtext = map["Rtext"];
                       	sendMessage(Rtext);//调用意图识别
           	   		} else {
           	   			alert("保存失败");
           	   		} 
                       break;
                   case 'error':
                       alert("上传失败");
                       break;
                   case 'cancel':
                       alert("上传被取消");
                       break;
               }
         });
	}
		
	//根据base64音频数据 转化为 blob对象
	function getBlob(base64Data){
		var dataURI = "data:audio/wav;base64,"+base64Data; //base64 字符串
	    var mimeString =  dataURI.split(',')[0].split(':')[1].split(';')[0]; // mime类型
	    var byteString = atob(dataURI.split(',')[1]); //base64 解码
	    var arrayBuffer = new ArrayBuffer(byteString.length); //创建缓冲数组
	    var intArray = new Uint8Array(arrayBuffer); //创建视图
	    for (i = 0; i < byteString.length; i += 1) {
	         intArray[i] = byteString.charCodeAt(i);
	    }
	    return new Blob([intArray], { type:  "audio/wav" }); //转成blob
	}        

	</script>
	
	<script>
		
		//发送按钮——文本提交消息
		$('.hot-search-btn').click(function(event){
			//获得发送的消息
		    var msg = $("#message").val();
			if(msg == ""){
				//若发送的消息为空
				return;
			}else{
				sendMessage(msg); //调用消息函数
			}
		});
		
		//添加发送的文本信息
		function sendMessage(msg){
			var htmlData =   '<div class="dialog_item dialog_client">'
                   + '   <div class="flex felxXEnd">'
			       + '   <div class="dialog_right">'
			       + '     <div class="dialog_content">' + msg + '</div>'
			       + '   </div>'
			       + '   <div class="dialog_left"><img src="../img/22.png" alt=""></div>'
			       + '</div>'  ;
			       
			$("#message_box").append(htmlData);
			$('#message_box').scrollTop($("#message_box")[0].scrollHeight + 20);
			$("#message").val('');
			
			var contentHeight = $(".dialog_wrap").height();
			$(".dialogBox").scrollTop(contentHeight); 
			//请求后端UNIT文本对话
			$.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/unit/getBot.do",
				data:{
					"msg":msg
				},
				success:function(data){
			   		var mes = eval(data);
			   		if (mes.success) {
			   			var say = data.say;
			   			var base64Data = data.base64Data;
			   			var audio2 = document.getElementById('aud');//用于播放合成语音
			   			audio2.src = window.URL.createObjectURL(getBlob(base64Data));//生成blob 路径
			   			audio2.autoplay = true;//播放
			   		 	//机器人回复样式加载
			   			var htmlData =   '<div class="dialog_item dialog_robot">'
			                + '   <div class="flex">'
						       + '   <div class="dialog_left">' 
						       + '	 <img src="../img/dialog-robot.png" alt=""></div>'
						       + '   <div class="dialog_right">'
			       			   + '     <div class="dialog_content">' + say + '</div>'
			       			   + '</div>'
						       + '</div>'  ;
						$("#message_box").append(htmlData);
						$('#message_box').scrollTop($("#message_box")[0].scrollHeight + 20);
						$("#message").val('');
						var contentHeight = $(".dialog_wrap").height();
						$(".dialogBox").scrollTop(contentHeight); 
			   		} else {
			   			alert("返回数据失败");
			   		}
				},
				error: function(){
			 		//请求出错处理
			 		alert("出情况了");
		 		}         
			});
		}
			
// 		var custom01 = new scrollbot(".dialogBox");
		animate(".voice-btn,.dialog-close,.dialog-icon,.dialog-btn >div");
		$(".voice-btn").click(function(){
			$(".dialog-coupon-content").show();
			$(".dialog-coupon").removeClass("none");
		})
		
		//点击麦克风开始录音
		$('.voice-btn').click(function(event){
			startRecording();//开始录音
		});
		
		//自动停止录音功能
		function recordStop() {
			$(".dialog-coupon-content").hide();
			$(".dialog-coupon").addClass("none");
			obtainRecord();//获取录音
			stopRecord();//停止录音
			uploadRecord();//上传录音
		}
		
		//关闭录音按钮
		$("dialog-coupon-content").click(function(){
			obtainRecord();//获取录音
			stopRecord();//停止录音
			uploadRecord();//上传录音
		})
	</script>
</body>
</html>